﻿using System;

namespace p3_lab4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int[][] tab = { new[] {1, 2, 3,},
                            new[] {6, 7, 8, 9, 1},
                            new[] {2, 3, 4, 5} 
                          };

            int tabLength = tab.Length;

            int[][] poszarpanyTab = new int[tabLength][];

            for (int i = 0; i < tabLength; i++) {
                poszarpanyTab[i] = new int[tab[i].Length];
                for (int j = 0; j < tab[i].Length; j++) {
                    poszarpanyTab[i][j] = tab[i][j];
                }
            }

            poszarpanyTab[2][3] = 404;
            poszarpanyTab[1][4] = 400;
            poszarpanyTab[0][1] = 510;

            foreach (var x in tab) {
                foreach (var y in x) {
                    Console.Write(y + " ");
                }

                Console.WriteLine();
            }

            Console.WriteLine("copy:");
            foreach (var x in poszarpanyTab) {
                foreach (var y in x) {
                    Console.Write(y + " ");
                }

                Console.WriteLine();
            }
        }
    }
}
